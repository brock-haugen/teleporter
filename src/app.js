// let's keep track of a few things
window.state = {
  id: null,
  key: null,
  link: (window.location.hash || "").split("#").pop(),
  logs: [],
  peers: [],
  ready: false
};

//
// START GLOBAL DEFINITIONS/LISTENERS

// setup our *local* IPFS
window.ipfsNode = new window.Ipfs({
  EXPERIMENTAL: {
    pubsub: true
  },
  config: {
    Addresses: {
      Swarm: [
        "/dns4/ws-star.discovery.libp2p.io/tcp/443/wss/p2p-websocket-star"
      ]
    }
  }
});

// wait for the node to be ready
window.ipfsNode.on("ready", function() {
  window.ipfsNode.id().then(function(idObj) {
    window.state.id = idObj.id;
    console.info("IPFS node ready!");
    window.setOrbit();
  });
});

// let's keep track of a few things
window.checker = function() {
  var done = function() {
    window.checkerTimeout = setTimeout(window.checker, 3000);
    m.redraw();
  };

  if (!state.ready) {
    done();
    return;
  }

  window.getPeers().then(function(peers) {
    window.state.peers = Array.isArray(peers) ? peers : [];
    window.state.logs = window.getLogs();
    done();
  });
};
window.checker();

//
// END GLOBAL DEFINITIONS/LISTENERS
//

//
// START ENCRYPTION METHODS
//

window.getCtr = function(key) {
  var keyBytes = window.aesjs.utils.utf8.toBytes(key || window.state.key);
  return new window.aesjs.ModeOfOperation.ctr(keyBytes, new aesjs.Counter(5));
};

window.encrypt = function(text, key) {
  var aesCtr = window.getCtr(key);
  var bytes = window.aesjs.utils.utf8.toBytes(text);
  var encrypted = aesCtr.encrypt(bytes);
  return window.aesjs.utils.hex.fromBytes(encrypted);
};

//
// END ENCRYPTION METHODS
//

//
// START IPFS METHODS
//

// teleport some data!
window.teleport = function(text, title) {
  // first we need a true buffer
  var buf = window.ipfsNode.types.Buffer.from(text);

  // then send it to IPFS
  window.ipfsNode.files.add(buf).then(function(resp) {
    try {
      var hash = resp[0].hash;

      // make sure we haven't seen this before
      var logs = window.getLogs();
      for (var i = 0; i < logs.length; i += 1) {
        if (logs[i].hash === hash) {
          window.toastr.success("File was already teleported!");
          return;
        }
      }

      // woo! we got a new hash
      console.info("Added a new hash!", hash);

      // now send that to OrbitDB
      window.db
        .add({
          _timestamp: Date.now(),
          encrypted: false,
          hash: hash,
          title: title,
          uploader: window.state.id
        })
        .then(function() {
          // the deed is done
          window.toastr.success("Successful teleportation!");
        });

      // also try getting ipfs.io to pick it up
      // we'll give it 10 minutes here before timing out
      window.axios
        .get("https://ipfs.io/ipfs/" + hash, {
          timeout: 10 * 60 * 1000
        })
        .catch(function() {
          return false;
        })
        .then(function(resp) {
          if (resp) {
            window.toastr.info("Synced file with https://ipfs.io");
          }
        });
    } catch (e) {
      console.error(e);
      window.toastr.error("Something went wrong in transmission...");
    }
  });
};

// who else are we teleporting with
window.getPeers = function() {
  var addr = window.db.address.toString();
  // this is a promise being returned
  return window.ipfsNode.pubsub.peers(addr);
};

// download file related to the IPFS hash
window.viewLog = function(log) {
  window.ipfsNode.files.cat(log.hash).then(function(uInt8Array) {
    try {
      var blob = new window.Blob([uInt8Array.buffer]);
      var url = window.URL.createObjectURL(blob);
      var downloadLink = document.createElement("a");
      downloadLink.href = url;
      downloadLink.download = log.title || log.hash;
      if (downloadLink.download.indexOf(".") < 0) {
        downloadLink.download += ".txt";
      }
      document.body.appendChild(downloadLink);
      downloadLink.click();
      document.body.removeChild(downloadLink);
    } catch (e) {
      console.error(e);
      window.toastr.error("Somethign went wrong fetching file...");
    }
  });
};

//
// END IPFS METHODS
//

//
// START ORBITDB METHODS
//

// let's get into an orbit
window.setOrbit = function() {
  // leverage IPFS here
  if (!window.orbitdb) {
    // only setup window.orbitdb once
    window.orbitdb = new OrbitDB(window.ipfsNode);
  }

  if (!window.state.link || window.state.link.length < 8) {
    // make sure we have a valid link
    window.state.link = null;
  }

  if (!window.state.link) {
    // looks like we haven't picked an orbit yet
    window.state.ready = true;
    m.redraw();
    return;
  }

  // make sure the URL is right
  window.location.hash = "#" + window.state.link;

  // try to connect to the db at the defined link
  // otherwise create it
  // then sync it all
  window.orbitdb
    .log(window.state.link, {
      create: true,
      localOnly: false,
      overwrite: false,
      sync: true,
      write: ["*"]
    })
    .then(function(db) {
      window.db = db;
      window.db.load().then(function() {
        // make sure we're pointed to the right place
        var fullOrbitLink = window.db.address.toString();
        if (window.location.hash !== "#" + fullOrbitLink) {
          window.location.hash = "#" + fullOrbitLink;
        }
        state.link = fullOrbitLink;

        // now we're cookin'
        window.state.ready = true;
        console.info("OrbitDB ready!");
        m.redraw();
      });
    });
};

// let's see what we know about so far
window.getLogs = function() {
  // collect the logs
  var logs = window.db.iterator({ limit: -1 }).collect();

  // we only care about valid payloads
  logs = logs
    .map(function(l) {
      return l.payload.value;
    })
    .filter(function(l) {
      return l._timestamp;
    });

  // sort by reverse chronological (newer first)
  logs.sort(function(a, b) {
    var v1 = a._timestamp;
    var v2 = b._timestamp;

    if (v1 < v2) return 1;
    if (v2 < v1) return -1;
    return 0;
  }, []);

  // give it all back
  return logs;
};

//
// END ORBITDB METHODS
//

// let's define a touch of app logic
var app = {
  view: function() {
    if (!window.state.ready) {
      return m("h3", "Getting things ready, this could take a few minutes...");
    }

    return [
      m("h3", "Teleporter"),
      m("em", "Transporting data from A to B without a C in between"),
      m("br"),
      m("br"),
      m("h5", ["You're ID is: ", m("code", window.state.id)]),
      window.state.link && [
        m("h5", ["You're connected to: ", m("code", window.state.link)]),
        m("h6", ["Peer count: ", m("code", window.state.peers.length)]),
        window.state.peers.length > 0 &&
          m("pre", m("code", window.state.peers.slice(0, 5).join("\n"))),
        window.state.logs.length > 0 && [
          m("h6", "Current files:"),
          window.state.logs.map(function(log) {
            return m(
              "a",
              {
                key: log._timestamp + "-" + log.hash,
                onclick: window.viewLog.bind(null, log),
                style: {
                  cursor: "pointer",
                  display: "block"
                }
              },
              log.title || log.hash
            );
          }),
          m("br"),
          m("br")
        ]
      ],
      m(
        "button",
        {
          onclick: function() {
            // get a fancy new link
            var newRandomLink = window
              .encrypt(
                window.state.id,
                Date.now().toString() +
                  Math.random()
                    .toString()
                    .slice(0, 3)
              )
              .slice(0, 16);

            // prompt for an override
            var newLink = window.prompt(
              "This is a new random link. Feel free to replace it with a known link or a new link of your choosing:",
              newRandomLink
            );

            if (newLink) {
              // set the new link
              window.state.link = newLink;
              window.setOrbit();
            }
          }
        },
        "Create or join another link"
      ),
      window.state.link && [
        m("br"),
        m("hr"),
        m("br"),
        m("h5", "Add file to teleport:"),
        m("input", {
          onchange: function() {
            try {
              var file = this.files[0];
              // setup a file reader to parse the added data into a string
              var reader = new window.FileReader();
              reader.onload = function(f) {
                var text = f.target.result;
                window.teleport(text, file.name);
              };
              reader.readAsArrayBuffer(file);
            } catch (e) {
              console.error(e);
              window.toastr.error("Something went wrong in transmission...");
            }
          },
          type: "file"
        }),
        m("br"),
        m("hr"),
        m("br")
      ]
    ];
  }
};

// now render it all!
m.mount(document.getElementById("app"), app);
